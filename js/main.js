"use strict";

// const element = (
//   <h1 class={"hello"} title="hi">
//     Hi there
//     <a href={"/"}>tag A text</a>
//   </h1>
// );

// that should transform to object
//
// const elementObj = {
//   type: "h1",
//   props: {
//     class: "hello",
//     title: "hi",
//     children: [
//       {
//         type: "TEXT_ELEMENT",
//         nodeValue: "Hi there",
//       },
//       {
//         type: "a",
//         props: {
//           href: "/",
//           children: { type: "TEXT_ELEMENT", nodeValue: "tag A text" },
//         },
//       },
//     ],
//   },
// };

// React.createElement();
//
// const elementAnother = <h1>text of h1</h1>;
//
// const container = document.getElementById("root");
//
// React.render(elementAnother, container);

function createElement(type, props, ...children) {
  return {
    type,
    props: {
      ...props,
      children: children.map((child) =>
        typeof child === "object" ? child : createTextElement(child)
      ),
    },
  };
}

function createTextElement(text) {
  return {
    type: "TEXT_ELEMENT",
    props: {
      nodeValue: text,
      children: [],
    },
  };
}

const myElement = createElement(
  "h1",
  {
    class: "hello",
    title: "hi",
  },
  "tag h1 text",
  createElement("a", { title: "title", children: "HI THERE" })
);

const container = document.getElementById("root");
console.log(container);
console.log(myElement);
// container.appendChild(JSON.stringify(myElement, null, 4));
